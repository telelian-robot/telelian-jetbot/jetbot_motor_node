from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, TextSubstitution
from launch_ros.actions import Node

def generate_launch_description():
    jetbot_motor_node_dir = get_package_share_directory('jetbot_motor_node')
    config_path = f'{jetbot_motor_node_dir}/config/config.yaml'
    
    use_sim_time = DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'
        )
    log_level = DeclareLaunchArgument(
            "log-level",
            default_value = TextSubstitution(text=str("INFO")),
            description="Logging level"
        )
    
    jetbot_motor_node = Node(
        package='jetbot_motor_node',
        executable='jetbot_motor_node',
        name='jetbot_motor',
        namespace='jetbot_ai',
        parameters=[config_path],
        arguments=['--ros-args', '--log-level', LaunchConfiguration('log-level')],
        output='screen')
    
    launchdesc = LaunchDescription([
        use_sim_time,
        log_level,
        jetbot_motor_node,
    ])
    
    return launchdesc

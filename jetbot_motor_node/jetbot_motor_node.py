import math

import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from rclpy.clock import Time, Duration, ClockType
from rcl_interfaces.msg import SetParametersResult

from geometry_msgs.msg import Twist, Quaternion, TransformStamped
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32, Int32

from tf2_ros import TransformBroadcaster

from .jetbot_motor.jetbot_motor.jetbot_motor import Motor

# python logging level
# CRITICAL    50
# ERROR       40
# WARNING     30
# INFO        20
# DEBUG       10
# NOTSET      0

PI = 3.141592

class Jetbot_Motor_Node(Node):
    def __init__(self):
        super().__init__('jetbot_motor_node')
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        
        self.declare_parameters(
            namespace='',
            parameters=[
                ('log-level', 'info'),
                ('i2c-bus', 7),
                ('left-motor-channel', 1),
                ('right-motor-channel', 2),
                ('max-speed', 0.5),
                ('width-vehicle', 0.095),
                ('radius-wheel', 0.032),
                ('speed-ratio', 0.2),
                ('cmd-vel-timeout', 0.2),
            ]
        )        
        self._log_level = self.get_logger().get_effective_level()
        self._i2c_bus = self.get_parameter('i2c-bus').value
        self._left_motor_channel = self.get_parameter('left-motor-channel').value
        self._right_motor_channel = self.get_parameter('right-motor-channel').value
        self._max_speed = self.get_parameter('max-speed').value
        self._max_speed = min(max(self._max_speed, -1.0), 1.0)
        
        self._width_vehicle = self.get_parameter('width-vehicle').value
        self._radius_wheel = self.get_parameter('radius-wheel').value
        self._speed_ratio = self.get_parameter('speed-ratio').value
        
        self.max_linear = self._max_speed / (self._radius_wheel*2*PI )
        self.max_angular = self._max_speed*self._width_vehicle*2*PI / (self._radius_wheel*2*PI )
        
        self._cmd_vel_timeout = self.get_parameter('cmd-vel-timeout').value
        
        self.get_logger().info(f'namespace: {self.get_namespace()}')
        self.get_logger().info(f'log-level: {self._log_level}')
        self.get_logger().info(f'i2c-bus: {self._i2c_bus}')
        self.get_logger().info(f'left-motor-channel: {self._left_motor_channel}')
        self.get_logger().info(f'right-motor-channel: {self._right_motor_channel}')
        self.get_logger().info(f'max-speed: {self._max_speed}')
        self.get_logger().info(f'width-vehicle: {self._width_vehicle}')
        self.get_logger().info(f'radius-wheel: {self._radius_wheel}')
        self.get_logger().info(f'speed-ratio: {self._speed_ratio}')
        self.get_logger().info(f'max-linear: {self.max_linear}')
        self.get_logger().info(f'max-angular: {self.max_angular}')
        
        self.add_on_set_parameters_callback(self.parameter_callback)
        
        self.cmdvel_sub = self.create_subscription(
            Twist,
            'cmd_vel',
            self.cmd_vel_callback,
            qos_profile
        )
        
        self.prev_cmd_vel_time = None
        
        self._l_motor = Motor(channel=self._left_motor_channel, i2c_bus=self._i2c_bus)
        self._r_motor = Motor(channel=self._right_motor_channel, i2c_bus=self._i2c_bus)
        
        self.timer = self.create_timer(0.1, self.timer_callback)
    
    def timer_callback(self):
        # period 100ms
        now_time = self.get_clock().now()
        if self.prev_cmd_vel_time is not None:    
            duration_cmd_vel = now_time - self.prev_cmd_vel_time
            duration_cmd_vel_sec = duration_cmd_vel.nanoseconds / 1e9
            if duration_cmd_vel_sec > self._cmd_vel_timeout:
                # self.get_logger().error(f'cmd_vel_callback timeout: {duration_cmd_vel_sec:.4f}s')
                self._l_motor._write_value(0.0)
                self._r_motor._write_value(0.0)
                return
            
        
    def cmd_vel_callback(self, msg):
        # now_time = Time(nanoseconds=msg.header.stamp.nanoseconds, seconds=msg.header.stamp.seconds)
        now_time = self.get_clock().now()
        if self.prev_cmd_vel_time is None:
            self.prev_cmd_vel_time = now_time
        else:
            duration = now_time - self.prev_cmd_vel_time 
            duration_sec = duration.nanoseconds / 1e9
            if duration_sec < self._cmd_vel_timeout:
                self.get_logger().debug(f'cmd_vel_callback too fast: {duration_sec:.4f}s') 
                return
            else:
                self.get_logger().debug(f'cmd_vel_callback duration: {duration_sec:.4f}s')
        
        linear = msg.linear.x
        angular = msg.angular.z
        linear = min(max(-self._max_speed, linear),self._max_speed)
        angular = min(max(-self._max_speed, angular),self._max_speed)   
        
        w_l = (linear - (angular*self._width_vehicle*2*PI)) / (self._radius_wheel*2*PI)
        w_r = (linear + (angular*self._width_vehicle*2*PI)) / (self._radius_wheel*2*PI)
        
        w_l = w_l / self.max_linear
        w_r = w_r / self.max_linear
        
        w_l = w_l * self._speed_ratio
        w_r = w_r * self._speed_ratio

        # if linear != 0.0:
        #     w_l = w_l / self.max_linear
        #     w_r = w_r / self.max_linear
        # else:
        #     w_l = w_l / self.max_angular
        #     w_r = w_r / self.max_angular
        
        self.get_logger().debug(f'{linear=} {angular=} {w_l=} {w_r=}')
        
        self._l_motor._write_value(w_l)
        self._r_motor._write_value(w_r)
        
        self.prev_cmd_vel_time = now_time
        
    def parameter_callback(self, params):
        for param in params:
            self.get_logger().info(f'parameter_callback: {param.name}, {param.type_}, {param.value}')
            if (param.name == 'log-level' 
                and param.type_ in (Parameter.Type.INTEGER, Parameter.Type.STRING)):
                self._log_level = param.value
                self._jetbot.set_debug_level(self._log_level)
                return SetParametersResult(successful=True)
            elif (param.name in ('max-speed', 'width-vehicle', 'redius-wheel', 'speed-ratio') 
                and param.type_ == (Parameter.Type.DOUBLE)):
                setattr(self, f'_{param.name}', param.value)
                self.max_linear = self._max_speed / (self._radius_wheel*2*PI )
                self.max_angular = self._max_speed*self._width_vehicle*2*PI / (self._radius_wheel*2*PI )
                return SetParametersResult(successful=True)
                
            
        return SetParametersResult(successful=False)
        
import sys
import argparse
import logging
def main(args=None):
    rclpy.init(args=args)
    node = Jetbot_Motor_Node()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.get_logger().info('Keyboard Interrupt (SIGINT)')
    finally:
        node.destroy_node()
    
if __name__ == '__main__':
    main()